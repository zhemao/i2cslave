library ieee;
use ieee.std_logic_1164.all;

entity v1tb4 is
end v1tb4;

architecture test of v1tb4 is 
	signal clk_hi		: std_logic		:= '1';
	signal sda_in		: std_logic		:= '0';
	signal scl_in		: std_logic		:= '1';
	signal ambs			: std_logic		:= '0';
	signal byte_done	: std_logic		:= '0';
	signal sda_out		: std_logic		:= '0';
	signal sda_enable	: std_logic		:= '0';
	signal addr_out	: std_logic		:= '0';
	signal data_out	: std_logic		:= '0';
	signal nack_sent	: std_logic		:= '0';
	
component i2cslave
		port (
		clk_hi		: in	std_logic;
		sda_in		: in	std_logic;
		scl_in		: in	std_logic;
		ambs			: in	std_logic;
		byte_done	: in	std_logic;
		sda_out		: out	std_logic;
		sda_enable	: out	std_logic;
		addr_out		: out	std_logic;
		data_out		: out	std_logic;
		nack_sent	: out	std_logic
		);
end component;

begin
	clk_hi	<= not clk_hi after 5 ns;
	scl_in	<= not scl_in after 50 ns;
	process
	begin
		-- 0 ns
		
		sda_in		<= '1';
		wait for 20 ns;
		sda_in		<= '0';
		wait for 30 ns;
		-- 50 ns
		
		--Address
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 150 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 250 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 350 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 450 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 550 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 650 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 750 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 850 ns
		
		-- Byte recevied
		byte_done	<= '1';
		ambs			<= '1';
		wait for 10 ns;
		byte_done	<= '0';
		ambs			<= '0';
		wait for 90 ns;
		-- 950 ns
		
		-- Send some random byte to this slave
		wait for 20 ns;
		ambs		<= '1';
		wait for 80 ns;
		-- 1050 ns
		
		wait for 20 ns;
		ambs		<= '1';
		wait for 80 ns;
		-- 1150 ns
		
		wait for 20 ns;
		ambs		<= '0';
		wait for 80 ns;
		-- 1250 ns
		
		wait for 20 ns;
		ambs		<= '1';
		wait for 80 ns;
		-- 1350 ns
		
		wait for 20 ns;
		ambs		<= '0';
		wait for 80 ns;
		-- 1450 ns
		
		wait for 20 ns;
		ambs		<= '1';
		wait for 80 ns;
		-- 1550 ns
		
		wait for 20 ns;
		ambs		<= '0';
		wait for 80 ns;
		-- 1650 ns
		
		wait for 20 ns;
		ambs		<= '0';
		wait for 80 ns;
		-- 1750 ns
		
		-- Byte Sent
		byte_done	<= '1';
		ambs			<= '0';
		wait for 10 ns;
		byte_done	<= '0';
		
		-- Start NACK from master
		sda_in		<= '1';
		wait for 90 ns;
		sda_in		<= '0';	
		-- End NACK from Master
		
		wait for 50 ns;
		-- 1900 ns
		
		-- Send STOP
		sda_in		<= '0';
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns; 
		-- 2000 ns
		
		wait;
	end process;
	
uut:	i2cslave
		port map(
		clk_hi		=> clk_hi,
		sda_in		=> sda_in,
		scl_in		=> scl_in,
		ambs			=> ambs,
		byte_done	=> byte_done,
		sda_out		=> sda_out,
		sda_enable	=> sda_enable,
		addr_out		=> addr_out,
		data_out		=> data_out,
		nack_sent	=> nack_sent
		);
end test;