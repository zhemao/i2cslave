library ieee;
use ieee.std_logic_1164.all;

entity i2cslave is
port (
	clk_hi		: in	std_logic;
	sda_in		: in	std_logic;
	scl_in		: in	std_logic;
	ambs			: in	std_logic;
	byte_done	: in	std_logic;
	sda_out		: out	std_logic;
	sda_enable	: out	std_logic;
	addr_out		: out	std_logic;
	data_out		: out	std_logic;
	nack_sent	: out	std_logic
	);
end i2cslave;

architecture v1 of i2cslave is
	type state_type is (
			S0,
			SS0,
			SS1,
			SS2,
			SS3,
			SS4,
			ADDR0,
			ADDR1,
			ACK0,
			ACK1,
			ACK2,
			ACK3,
			ACK4,
			ACK5,
			ACK6,
			ACK7,
			NACK0,
			NACK1,
			SEND0,
			SEND1,
			RECV0,
			RECV1,
			RECV2,
			RECV3
	);
	signal state	: state_type;
begin
	process(clk_hi)
	begin
		if (clk_hi'event and clk_hi = '1') then
			case state is
			when S0		=>
				if		(sda_in = '0' or scl_in = '0') then
					state	<= S0;
				elsif (sda_in = '1' and scl_in = '1') then
					state	<= SS3;
				end if;

			when SS0		=>
				if		(scl_in = '0') then
					state	<= SS0;
				elsif	(scl_in = '1' and sda_in = '1') then
					state	<= SS2;
				elsif	(scl_in = '1' and sda_in = '0') then
					state	<= SS1;
				end if;

			when SS1		=>
				if		(scl_in = '1' and sda_in = '0') then
					state	<= SS1;
				elsif	(scl_in = '1' and sda_in = '1') then
					state	<= SS3;
				elsif	(scl_in = '0') then
					state	<= SS0;
				end if;

			when SS2		=>
				if		(scl_in = '1' and sda_in = '1') then
					state	<= SS2;
				elsif	(scl_in = '1' and sda_in = '0') then
					state	<= SS4;
				elsif	(scl_in = '0') then
					state	<= SS0;
				end if;

			when SS3		=>
				if		(scl_in = '1' and sda_in = '1') then
					state	<= SS3;
				elsif	(scl_in = '1' and sda_in = '0') then
					state	<= SS4;
				elsif	(scl_in = '0') then
					state	<= S0;
				end if;

			when SS4		=>
				if		(scl_in = '1' and sda_in = '0') then
					state	<= SS4;
				elsif	(scl_in = '1' and sda_in = '1') then
					state	<= SS3;
				elsif	(scl_in = '0' and sda_in = '1') then
					state	<= ADDR0;
				elsif	(scl_in = '0' and sda_in = '0') then
					state	<= ADDR1;
				end if;

			when ADDR0	=>
				if		(scl_in = '0' and byte_done = '0') then
					state	<= ADDR0;
				elsif	(sda_in = '1' and scl_in = '1' and byte_done = '0') then
					state	<= ADDR0;
				elsif	(sda_in = '0' and scl_in = '1' and byte_done = '0') then
					state	<= ADDR1;
				elsif	(byte_done = '1' and ambs = '1' and scl_in = '0') then
					state	<= ACK0;
				elsif	(byte_done = '1' and ambs = '0' and scl_in = '0') then
					state	<= SS0;
				end if;

			when ADDR1	=>
				if		(scl_in = '0' and byte_done = '0') then
					state	<= ADDR1;
				elsif	(sda_in = '0' and scl_in = '1' and byte_done = '0') then
					state	<= ADDR1;
				elsif	(sda_in = '1' and scl_in = '1' and byte_done = '0') then
					state <= ADDR0;
				elsif	(byte_done = '1' and ambs = '1' and scl_in = '0') then
					state	<= ACK2;
				elsif	(byte_done = '1' and ambs = '0' and scl_in = '0') then
					state	<= SS0;
				end if;

			when ACK0	=>
				if		(scl_in = '0') then
					state <= ACK0;
				elsif	(scl_in = '1') then
					state <= ACK1;
				end if;

			when ACK1	=>
				if		(scl_in = '1') then
					state <= ACK1;
				elsif	(scl_in = '0' and ambs = '1') then
					state <= SEND0;
				elsif	(scl_in = '0' and ambs = '0') then
					state <= SEND1;
				end if;

			when ACK2	=>
				if		(scl_in = '0') then
					state <= ACK2;
				elsif	(scl_in = '1') then
					state <= ACK3;
				end if;

			when ACK3	=>
				if		(scl_in = '1') then
					state <= ACK3;
				elsif	(scl_in = '0' and sda_in = '0') then
					state <= RECV0;
				elsif	(scl_in = '0' and sda_in = '1') then
					state <= RECV1;
				end if;

			when ACK4	=>
				if		(scl_in = '0' and sda_in = '0') then
					state <= ACK4;
				elsif	(scl_in = '1' and sda_in = '0') then
					state <= ACK5;
				elsif	(scl_in = '0' and sda_in = '1') then
					state	<= NACK0;
				end if;

			when ACK5	=>
				if		(scl_in = '1' and sda_in = '0') then
					state <= ACK5;
				elsif	(scl_in = '0' and ambs = '1') then
					state <= SEND0;
				elsif	(scl_in = '0' and ambs = '0') then
					state <= 	SEND1;
				end if;

			when ACK6	=>
				if		(scl_in = '0') then
					state <= ACK6;
				elsif	(scl_in = '1') then
					state <= ACK7;
				end if;

			when ACK7	=>
				if		(scl_in = '1') then
					state <= ACK7;
				elsif	(scl_in = '0' and sda_in = '0') then
					state <= RECV0;
				elsif	(scl_in = '0' and sda_in = '1') then
					state <= RECV1;
				end if;

			when NACK0	=>
				if		(scl_in = '0' and sda_in = '1') then
					state <= NACK0;
				elsif	(scl_in = '1' and sda_in = '1') then
					state <= NACK1;
				elsif	(scl_in = '0' and sda_in = '0') then
					state	<= ACK4;
				end if;

			when NACK1	=>
				if		(scl_in = '1' and sda_in = '1') then
					state <= NACK1;
				elsif	(scl_in = '0') then
					state <= SS0;
				end if;

			when SEND0	=>
				if		(scl_in = '1') then
					state <= SEND0;
				elsif	(scl_in = '0' and ambs = '1' and byte_done = '0') then
					state <= SEND0;
				elsif	(scl_in = '0' and ambs = '0' and byte_done = '0') then
					state <= SEND1;
				elsif	(byte_done = '1' and scl_in = '0') then
					state <= ACK4;
				end if;

			when SEND1	=>
				if		(scl_in = '1') then
					state <= SEND1;
				elsif	(scl_in = '0' and ambs = '0' and byte_done = '0') then
					state <= SEND1;
				elsif	(scl_in = '0' and ambs = '1' and byte_done = '0') then
					state <= SEND0;
				elsif	(byte_done = '1' and scl_in = '0') then
					state <= ACK4;
				end if;

			when RECV0	=>
				if		(scl_in = '0' and sda_in = '0' and byte_done = '0') then
					state <= RECV0;
				elsif	(scl_in = '0' and sda_in = '1' and byte_done = '0') then
					state <= RECV1;
				elsif	(scl_in = '1' and sda_in = '0') then
					state <= RECV2;
				elsif	(byte_done = '1') then
					state <= ACK6;
				end if;

			when RECV1	=>
			if			(scl_in = '0' and sda_in = '1' and byte_done = '0') then
					state <= RECV1;
				elsif	(scl_in = '0' and sda_in = '0' and byte_done = '0') then
					state <= RECV0;
				elsif	(scl_in = '1' and sda_in = '1') then
					state <= RECV3;
				elsif	(byte_done = '1') then
					state <= ACK6;
				end if;

			when RECV2	=>
				if		(scl_in = '1' and sda_in = '0') then
					state <= RECV2;
				elsif	(scl_in = '0' and sda_in = '0') then
					state <= RECV0;
				elsif	(scl_in = '1' and sda_in = '1') then
					state <= SS3;
				end if;

			when RECV3	=>
				if		(scl_in = '1' and sda_in = '1') then
					state <= RECV3;
				elsif	(scl_in = '0' and sda_in = '1') then
					state <= RECV1;
				end if;
			end case;
		end if;
	end process;
	
	sda_out		<= '1' when (state = SEND0) 
						else '0';
	
	sda_enable 	<= '1' when (state = ACK0)		or
									(state = ACK1)		or
									(state = SEND0)	or
									(state = SEND1)	or
									(state = ACK2)		or
									(state = ACK3)		or
									(state = ACK6)		or
									(state = ACK7)
						else '0';
						
	data_out		<= '1' when (state = RECV1)	or
									(state = RECV3)
						else '0';
	
	addr_out		<= '1' when (state = ADDR0)
						else '0';
						
	nack_sent	<= '0';
	
end v1;