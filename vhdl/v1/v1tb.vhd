library ieee;
use ieee.std_logic_1164.all;

entity v1tb is
end v1tb;

architecture test of v1tb is 
	signal clk_hi		: std_logic		:= '0';
	signal sda_in		: std_logic		:= '0';
	signal scl_in		: std_logic		:= '0';
	signal ambs			: std_logic		:= '0';
	signal byte_done	: std_logic		:= '0';
	signal sda_out		: std_logic		:= '0';
	signal sda_enable	: std_logic		:= '0';
	signal addr_out	: std_logic		:= '0';
	signal data_out	: std_logic		:= '0';
	signal nack_sent	: std_logic		:= '0';
	
component i2cslave
		port (
		clk_hi		: in	std_logic;
		sda_in		: in	std_logic;
		scl_in		: in	std_logic;
		ambs			: in	std_logic;
		byte_done	: in	std_logic;
		sda_out		: out	std_logic;
		sda_enable	: out	std_logic;
		addr_out		: out	std_logic;
		data_out		: out	std_logic;
		nack_sent	: out	std_logic
		);
end component;
begin
	clk_hi	<= not clk_hi after 1 ns;
	process
	begin
		wait for 10 ns;
		sda_in	<= '1';
		scl_in	<= '1';
		
		wait for 10 ns;
		sda_in	<= '0';
		scl_in		<= '1';
		
		wait for 10 ns;
		sda_in	<= '1';
		scl_in		<= '0';
		
		wait;
	end process;
	
uut:	i2cslave
		port map(
		clk_hi		=> clk_hi,
		sda_in		=> sda_in,
		scl_in		=> scl_in,
		ambs			=> ambs,
		byte_done	=> byte_done,
		sda_out		=> sda_out,
		sda_enable	=> sda_enable,
		addr_out		=> addr_out,
		data_out		=> data_out,
		nack_sent	=> nack_sent
		);
end test;