library ieee;
use ieee.std_logic_1164.all;

entity i2cslave is
port (
	clk_hi		: in	std_logic;
	sda_in		: in	std_logic;
	scl_in		: in	std_logic;
	ambs			: in	std_logic;
	byte_done	: in	std_logic;
	sda_out		: out	std_logic;
	sda_enable	: out	std_logic;
	addr_out		: out	std_logic;
	data_out		: out	std_logic;
	nack_sent	: out	std_logic
	);
end i2cslave;

architecture v1 of i2cslave is
	type state_type is (
			S0,
			SS0,
			SS1,
			SS2,
			SS3,
			SS4,
			ADDR0,
			ADDR1,
			ACK0,
			ACK1,
			ACK2,
			ACK3,
			ACK4,
			ACK5,
			ACK6,
			ACK7,
			NACK0,
			NACK1,
			NACK2,
			NACK3,
			NACK4,
			SEND0,
			SEND1,
			SEND2,
			SEND3,
			SEND4,
			SEND5,
			SEND6,
			SEND7,
			SEND8,
			SEND9,
			SEND10,
			SEND11,
			RECV0,
			RECV1,
			RECV2,
			RECV3,
			RECV4,
			RECV5,
			RECV6,
			RECV7
	);
	signal state	: state_type;
begin
	process(clk_hi)
	begin
		if (clk_hi'event and clk_hi = '1') then
			case state is
			when S0		=>
				if		(sda_in = '0' or scl_in = '0') then
					state	<= S0;
				elsif (sda_in = '1' and scl_in = '1') then
					state	<= SS3;
				end if;

			when SS0		=>
				if		(scl_in = '0') then
					state	<= SS0;
				elsif	(scl_in = '1' and sda_in = '1') then
					state	<= SS2;
				elsif	(scl_in = '1' and sda_in = '0') then
					state	<= SS1;
				end if;

			when SS1		=>
				if		(scl_in = '1' and sda_in = '0') then
					state	<= SS1;
				elsif	(scl_in = '1' and sda_in = '1') then
					state	<= SS3;
				elsif	(scl_in = '0') then
					state	<= SS0;
				end if;

			when SS2		=>
				if		(scl_in = '1' and sda_in = '1') then
					state	<= SS2;
				elsif	(scl_in = '1' and sda_in = '0') then
					state	<= SS4;
				elsif	(scl_in = '0') then
					state	<= SS0;
				end if;

			when SS3		=>
				if		(scl_in = '1' and sda_in = '1') then
					state	<= SS3;
				elsif	(scl_in = '1' and sda_in = '0') then
					state	<= SS4;
				elsif	(scl_in = '0') then
					state	<= S0;
				end if;

			when SS4		=>
				if		(scl_in = '1' and sda_in = '0') then
					state	<= SS4;
				elsif	(scl_in = '1' and sda_in = '1') then
					state	<= SS3;
				elsif	(scl_in = '0' and sda_in = '1') then
					state	<= ADDR0;
				elsif	(scl_in = '0' and sda_in = '0') then
					state	<= ADDR1;
				end if;

			when ADDR0	=>
				if		(scl_in = '0' and byte_done = '0') then
					state	<= ADDR0;
				elsif	(sda_in = '1' and scl_in = '1' and byte_done = '0') then
					state	<= ADDR0;
				elsif	(sda_in = '0' and scl_in = '1' and byte_done = '0') then
					state	<= ADDR1;
				elsif	(byte_done = '1' and ambs = '1' and scl_in = '0') then
					state	<= ACK0;
				elsif	(byte_done = '1' and ambs = '0' and scl_in = '0') then
					state	<= SS0;
				end if;

			when ADDR1	=>
				if		(scl_in = '0' and byte_done = '0') then
					state	<= ADDR1;
				elsif	(sda_in = '0' and scl_in = '1' and byte_done = '0') then
					state	<= ADDR1;
				elsif	(sda_in = '1' and scl_in = '1' and byte_done = '0') then
					state <= ADDR0;
				elsif	(byte_done = '1' and ambs = '1' and scl_in = '0') then
					state	<= ACK2;
				elsif	(byte_done = '1' and ambs = '0' and scl_in = '0') then
					state	<= SS0;
				end if;

			when ACK0	=>
				if		(scl_in = '0') then
					state <= ACK0;
				elsif	(scl_in = '1') then
					state <= ACK1;
				end if;

			when ACK1	=>
				if		(scl_in = '1') then
					state <= ACK1;
				elsif	(scl_in = '0' and ambs = '1') then
					state <= SEND0;
				elsif	(scl_in = '0' and ambs = '0') then
					state <= SEND1;
				end if;

			when ACK2	=>
				if		(scl_in = '0') then
					state <= ACK2;
				elsif	(scl_in = '1') then
					state <= ACK3;
				end if;

			when ACK3	=>
				if		(scl_in = '1') then
					state <= ACK3;
				elsif	(scl_in = '0' and sda_in = '0') then
					state <= RECV0;
				elsif	(scl_in = '0' and sda_in = '1') then
					state <= RECV1;
				end if;

			when ACK4	=>
				if		(scl_in = '0' and sda_in = '0') then
					state <= ACK4;
				elsif	(scl_in = '1' and sda_in = '0') then
					state <= ACK5;
				elsif	(scl_in = '0' and sda_in = '1') then
					state	<= NACK0;
				end if;

			when ACK5	=>
				if		(scl_in = '1' and sda_in = '0') then
					state <= ACK5;
				elsif	(scl_in = '0' and ambs = '1') then
					state <= SEND0;
				elsif	(scl_in = '0' and ambs = '0') then
					state <= SEND1;
				end if;

			when ACK6	=>
				if		(scl_in = '0') then
					state <= ACK6;
				elsif	(scl_in = '1') then
					state <= ACK7;
				end if;

			when ACK7	=>
				if		(scl_in = '1') then
					state <= ACK7;
				elsif	(scl_in = '0' and sda_in = '0') then
					state <= RECV0;
				elsif	(scl_in = '0' and sda_in = '1') then
					state <= RECV1;
				end if;

			when NACK0	=>
				if		(scl_in = '0' and sda_in = '1') then
					state <= NACK0;
				elsif	(scl_in = '1' and sda_in = '1') then
					state <= NACK1;
				elsif	(scl_in = '0' and sda_in = '0') then
					state	<= ACK4;
				end if;

			when NACK1	=>
				if		(scl_in = '1' and sda_in = '1') then
					state <= NACK1;
				elsif	(scl_in = '0') then
					state <= NACK4;
				end if;

			when NACK2	=>
				if		(scl_in = '0') then
					state	<= NACK2;
				elsif	(scl_in = '1') then
					state	<= NACK3;
				end if;

			when NACK3	=>
				if		(scl_in = '1') then
					state	<= NACK3;
				elsif	(scl_in = '0') then
					state	<= NACK4;
				end if;
				
			when NACK4	=>
				if		(scl_in = '0') then
					state	<= SS0;
				end if;

			when SEND0	=>
				if		(scl_in = '0' and ambs = '1' and byte_done = '0') then
					state	<= SEND0;
				elsif	(scl_in = '0' and ambs = '0' and byte_done = '0') then
					state	<= SEND1;
				elsif	(scl_in = '1' and byte_done = '0') then
					state	<= SEND2;
				elsif	(byte_done = '1') then
					state	<= SEND8;
				end if;

			when SEND1	=>
				if		(scl_in = '0' and ambs = '0' and byte_done = '0') then
					state	<= SEND1;
				elsif	(scl_in = '0' and ambs = '1' and byte_done = '0') then
					state	<= SEND0;
				elsif	(scl_in = '1' and byte_done = '0') then
					state	<= SEND3;
				elsif	(byte_done = '1') then
					state	<= SEND8;
				end if; 
			
			when SEND2	=>
				if		(scl_in = '1') then
					state	<= SEND2;
				elsif	(scl_in = '0') then
					state	<= SEND4;
				end if;
			
			when SEND3	=>
				if		(scl_in = '1') then
					state	<= SEND3;
				elsif	(scl_in = '0') then
					state	<= SEND1;
				end if;
			
			when SEND4	=>
				if		(scl_in = '0' and ambs = '1' and byte_done = '0') then
					state	<= SEND4;
				elsif	(scl_in = '0' and ambs = '0' and byte_done = '0') then
					state	<= SEND5;
				elsif	(scl_in = '1' and byte_done = '0') then
					state	<= SEND6;
				elsif	(byte_done = '1') then
					state	<= SEND10;
				end if;
			
			when SEND5	=>
				if		(scl_in = '0' and ambs = '0' and byte_done = '0') then
					state	<= SEND5;
				elsif	(scl_in = '0' and ambs = '1' and byte_done = '0') then
					state	<= SEND4;
				elsif	(scl_in = '1' and byte_done = '0') then
					state	<= SEND7;
				elsif	(byte_done = '1') then
					state	<= SEND10;
				end if;
			
			when SEND6	=>
				if		(scl_in = '1') then
					state	<= SEND6;
				elsif	(scl_in = '0') then
					state	<= SEND0;
				end if;
			
			when SEND7	=>
				if		(scl_in = '1') then
					state	<= SEND7;
				elsif	(scl_in = '0') then
					state	<= SEND5;
				end if;
			
			when SEND8	=>
				if		(scl_in = '0') then
					state	<= SEND8;
				elsif	(scl_in = '1') then
					state	<= SEND9;
				end if;
			
			when SEND9	=>
				if		(scl_in = '1') then
					state	<= SEND9;
				elsif	(scl_in = '0') then
					state	<= ACK4;
				end if;
			
			when SEND10	=>
				if		(scl_in = '0') then
					state	<= SEND10;
				elsif	(scl_in = '1') then
					state	<= SEND11;
				end if;
			
			when SEND11	=>
				if		(scl_in = '1') then
					state	<= SEND11;
				elsif	(scl_in = '0') then
					state	<= ACK4;
				end if;

			when RECV0	=>
				if		(scl_in = '0' and sda_in = '0' and byte_done = '0') then
					state	<= RECV0;
				elsif	(scl_in = '0' and sda_in = '1' and byte_done = '0') then
					state	<= RECV1;
				elsif	(scl_in = '1' and byte_done = '0') then
					state	<= RECV2;
				elsif	(byte_done = '1') then
					state	<= ACK6;
				end if;

			when RECV1	=>
				if		(scl_in = '0' and sda_in = '1' and byte_done = '0') then
					state	<= RECV1;
				elsif	(scl_in = '0' and sda_in = '0' and byte_done = '0') then
					state	<= RECV0;
				elsif	(scl_in = '1' and byte_done = '0') then
					state	<= RECV3;
				elsif	(byte_done = '1') then
					state	<= ACK6;
				end if;

			when RECV2	=>
				if		(scl_in = '1' and sda_in = '0') then
					state	<= RECV2;
				elsif	(scl_in = '0') then
					state	<= RECV0;
				elsif	(scl_in = '1' and sda_in = '1') then
					state	<= SS3;
				end if;

			when RECV3	=>
				if		(scl_in = '1' and sda_in = '1') then
					state	<= RECV3;
				elsif	(scl_in = '0') then
					state	<= RECV5;
				end if;
			
			when RECV4	=>
				if		(scl_in = '0' and sda_in = '0' and byte_done = '0') then
					state	<= RECV4;
				elsif	(scl_in = '0' and sda_in = '1' and byte_done = '0') then
					state	<= RECV5;
				elsif	(scl_in = '1' and byte_done = '0') then
					state	<= RECV6;
				elsif	(byte_done = '1') then
					state	<= NACK2;
				end if;
			
			when RECV5	=>
				if		(scl_in = '0' and sda_in = '1' and byte_done = '0') then
					state	<= RECV5;
				elsif	(scl_in = '0' and sda_in = '0' and byte_done = '0') then
					state	<= RECV4;
				elsif	(scl_in = '1' and byte_done = '0') then
					state	<= RECV7;
				elsif	(byte_done = '1') then
					state	<= NACK2;
				end if;
			
			when RECV6	=>
				if		(scl_in = '1' and sda_in = '0') then
					state	<= RECV6;
				elsif	(scl_in = '0') then
					state	<= RECV4;
				elsif	(scl_in = '1' and sda_in = '1') then
					state	<= SS3;
				end if;
			
			when RECV7	=>
				if		(scl_in = '1' and sda_in = '1') then
					state	<= RECV7;
				elsif	(scl_in = '0') then
					state	<= RECV1;
				end if;
			end case;
		end if;
	end process;
	
	sda_out		<= '1' when (state = SEND0)	or
									(state = SEND2)	or
									(state = SEND4)	or
									(state = SEND6)	or
									(state = SEND10)	or
									(state = SEND11)	or
									(state = NACK2)	or
									(state = NACK3)
						else '0';
	
	sda_enable 	<= '1' when (state = ACK0)		or
									(state = ACK1)		or
									(state = ACK2)		or
									(state = ACK3)		or
									(state = ACK6)		or
									(state = ACK7)		or
									(state = NACK2)	or
									(state = NACK3)	or
									(state = SEND0)	or
									(state = SEND1)	or
									(state = SEND2)	or
									(state = SEND3)	or
									(state = SEND4)	or
									(state = SEND5)	or
									(state = SEND6)	or
									(state = SEND7)	or
									(state = SEND8)	or
									(state = SEND9)	or
									(state = SEND10)	or
									(state = SEND11)
						else '0';
						
	data_out		<= '1' when (state = RECV1)	or
									(state = RECV3)	or
									(state = RECV5)	or
									(state = RECV7)
						else '0';
	
	addr_out		<= '1' when (state = ADDR0)
						else '0';
						
	nack_sent	<= '1' when (state = NACK4)
						else '0';
	
end v1;
