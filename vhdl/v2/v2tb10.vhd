library ieee;
use ieee.std_logic_1164.all;

entity v2tb10 is
end v2tb10;

architecture test of v2tb10 is 
	signal clk_hi		: std_logic		:= '1';
	signal sda_in		: std_logic		:= '0';
	signal scl_in		: std_logic		:= '1';
	signal ambs			: std_logic		:= '0';
	signal byte_done	: std_logic		:= '0';
	signal sda_out		: std_logic		:= '0';
	signal sda_enable	: std_logic		:= '0';
	signal addr_out	: std_logic		:= '0';
	signal data_out	: std_logic		:= '0';
	signal nack_sent	: std_logic		:= '0';
	
component i2cslave
		port (
		clk_hi		: in	std_logic;
		sda_in		: in	std_logic;
		scl_in		: in	std_logic;
		ambs			: in	std_logic;
		byte_done	: in	std_logic;
		sda_out		: out	std_logic;
		sda_enable	: out	std_logic;
		addr_out		: out	std_logic;
		data_out		: out	std_logic;
		nack_sent	: out	std_logic
		);
end component;

begin
	clk_hi	<= not clk_hi after 5 ns;
	scl_in	<= not scl_in after 50 ns;
	process
	begin
		-- 0 ns
		sda_in		<= '1';
		wait for 20 ns;
		sda_in		<= '0';
		wait for 30 ns;
		-- 50 ns
		
		--Address
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 150 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 250 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 350 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 450 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 550 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 650 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 750 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 850 ns
		
		-- Byte recevied
		byte_done	<= '1';
		ambs			<= '1';
		wait for 10 ns;
		byte_done	<= '0';
		ambs			<= '0';
		wait for 90 ns;
		-- 950 ns
		
		-- Send some random byte with right parity to this slave
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 1050 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 1150 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 1250 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 1350 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 1450 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 1550 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 1650 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 1750 ns
		
		-- Byte received by slave	
		byte_done	<= '1';
		wait for 10 ns;
		byte_done	<= '0';
		wait for 90 ns;		
		-- 1750 ns
		
		wait for 50 ns;
		
		-- Send STOP then START
		sda_in		<= '0';
		wait for 15 ns;
		sda_in		<= '1';
		wait for 15 ns; 
		sda_in		<= '0';
		wait for 20 ns;
		-- 1850 ns
		
		--Address
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 1950 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 2050 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 2150 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 2250 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 2350 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 2450 ns
		
		wait for 20 ns;
		sda_in		<= '0';
		wait for 80 ns;
		-- 2550 ns
		
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 2650 ns
		
		-- Byte recevied
		byte_done	<= '1';
		ambs			<= '1';
		wait for 10 ns;
		byte_done	<= '0';
		ambs			<= '0';
		wait for 90 ns;
		-- 2750 ns
		
		-- Send some random byte to from slave
		
		wait for 20 ns;
		ambs		<= '1';
		wait for 80 ns;
		-- 2850 ns
		
		wait for 20 ns;
		ambs		<= '1';
		wait for 80 ns;
		-- 2950 ns
		
		wait for 20 ns;
		ambs		<= '0';
		wait for 80 ns;
		-- 3050 ns
		
		wait for 20 ns;
		ambs		<= '1';
		wait for 80 ns;
		-- 3150 ns
		
		wait for 20 ns;
		ambs		<= '0';
		wait for 80 ns;
		-- 3250 ns
		
		wait for 20 ns;
		ambs		<= '1';
		wait for 80 ns;
		-- 3350 ns
		
		wait for 20 ns;
		ambs		<= '0';
		wait for 80 ns;
		-- 3450 ns
		
		-- 7th bit sent to master
		byte_done	<= '1';
		ambs			<= '0';
		wait for 10 ns;		
		byte_done	<= '0';
		
		-- Waiting for parity bit to be sent
		wait for 90 ns;
		-- 3550 ns
		
		wait for 10 ns;
		
		-- Start NACK from master
		sda_in		<= '1';
		wait for 90 ns;
		sda_in		<= '0';
		-- End NACK from Master
		
		wait for 50 ns;
		-- 3700 ns
		
		-- Send STOP
		sda_in		<= '0';
		wait for 20 ns;
		sda_in		<= '1';
		wait for 80 ns;
		-- 3800 ns
		
		wait;
	end process;
	
uut:	i2cslave
		port map(
		clk_hi		=> clk_hi,
		sda_in		=> sda_in,
		scl_in		=> scl_in,
		ambs			=> ambs,
		byte_done	=> byte_done,
		sda_out		=> sda_out,
		sda_enable	=> sda_enable,
		addr_out		=> addr_out,
		data_out		=> data_out,
		nack_sent	=> nack_sent
		);
end test;
